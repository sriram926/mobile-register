package com.register.mobileregister;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.app.Activity;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MobileregisterActivity extends Activity {
    /** Called when the activity is first created. */
	Button btnLogin,btnStudent;
	EditText uname,pass; 
    @Override
    public void onCreate(Bundle savedInstanceState) {
       	super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        
        uname=(EditText)findViewById(R.id.edtUname);
        pass=(EditText)findViewById(R.id.edtPass);
        
        btnLogin=(Button)findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
		
				/* start of Validation */
				
				//creating name values pair to send data to server
				ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
				postParameters.add(new BasicNameValuePair("uname",uname.getText().toString().trim()));
				postParameters.add(new BasicNameValuePair("pass",pass.getText().toString().trim()));
            	
            	InputStream is =null;
            	String result = "";
            	
            	
            	//Sending Information to server
            	try {
            	    
            		HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost("http://10.0.2.2:8080/RegisterServer/ValidateLogin");
                    httppost.setEntity(new UrlEncodedFormEntity(postParameters));
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();
                    is = entity.getContent();         	    
            	} 
            	catch (Exception e) {
            		
            		Toast.makeText(MobileregisterActivity.this,"Error in http connection : "+e.toString(), Toast.LENGTH_LONG).show();
            	}
            	
            	
            	//Processing results given by server
            	//convert response to string
                try{
                        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
                        StringBuilder sb = new StringBuilder();
                        String line = null;
                        while ((line = reader.readLine()) != null) {
                                sb.append(line + "\n");
                        }
                        is.close();
                 
                        result=sb.toString();
 //                       Toast.makeText(MobileregisterActivity.this,"Message : "+result+result.length(), Toast.LENGTH_LONG).show();
                    
                        /*End of Validation*/
    					if(result.trim().equalsIgnoreCase("no"))
    					{
    						Toast.makeText(MobileregisterActivity.this,"Invalid username : ", Toast.LENGTH_LONG).show();
    					}
    					else if(result.trim().equalsIgnoreCase("yes"))
    					{
    						try
    						{
    							Intent in=new Intent(MobileregisterActivity.this,lecturerHomeActivity.class);
    							
    							Bundle bu=new Bundle();
    							bu.putString("cls_selected", "4-1");//add selected class here
    							bu.putString("userid", uname.getText().toString().trim());
    							in.putExtras(bu);
    							
    							
    							startActivity(in);
    						}
    						catch(Exception e)
    						{
    							Toast.makeText(MobileregisterActivity.this,"Exc : "+e.toString(), 10).show();
    						}
    				}
    					else
    					{
    						Toast.makeText(MobileregisterActivity.this,"Error encountered"+result+result.length(), Toast.LENGTH_LONG).show();
    					}
                
                }
                catch (Exception e) 
                {
            		
            		Toast.makeText(MobileregisterActivity.this,"Error converting result : "+e.toString(), Toast.LENGTH_LONG).show();
            	}
            	

				
				
			}
		});
        
        
        
        
        btnStudent=(Button)findViewById(R.id.btnStudentLogin);
        btnStudent.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				try
				{
					Intent studLogin=new Intent(MobileregisterActivity.this,student1Activity.class);
					startActivity(studLogin);
				}
				catch(Exception e)
				{
					Toast.makeText(MobileregisterActivity.this,"Exc : "+e.toString(), 10).show();
				}
			}
		});
    
    }
}