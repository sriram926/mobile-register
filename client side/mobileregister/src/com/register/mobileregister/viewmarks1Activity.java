package com.register.mobileregister;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class viewmarks1Activity  extends Activity {
	Button btnViewMarks;
	String userid,subid,semid,secid;
	 public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.viewmarks1);
	        btnViewMarks=(Button)findViewById(R.id.btnViewMarks);
	        
	        Bundle bu=getIntent().getExtras();
	        userid=bu.getString("userid");
	        semid=bu.getString("semid");
	        secid=bu.getString("secid");
	        subid=bu.getString("subid");
	        
	        Toast.makeText(viewmarks1Activity.this,userid, 10).show();
	        
	        String[] exams={"Desc1"," Quiz1","Desc2","Quiz2"};
	        
	        final Spinner spinner = (Spinner) findViewById(R.id.spinExam);
	        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, exams);
	        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	        spinner.setAdapter(adapter);
	        
	        btnViewMarks.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
				
					
					//creating name values pair to send data to server
					ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
					postParameters.add(new BasicNameValuePair("userid",userid.trim()));
					postParameters.add(new BasicNameValuePair("semid",semid.trim()));
					postParameters.add(new BasicNameValuePair("subid",subid.trim()));
					postParameters.add(new BasicNameValuePair("secid",secid.trim()));
					
					String k="";
					if(spinner.getSelectedItem().toString().equalsIgnoreCase("desc1"))
						k="1";
					else if(spinner.getSelectedItem().toString().equalsIgnoreCase("quiz1"))
						k="2";
					else if(spinner.getSelectedItem().toString().equalsIgnoreCase("desc2"))
						k="3";
					else if(spinner.getSelectedItem().toString().equalsIgnoreCase("quiz2"))
						k="4";
					
					postParameters.add(new BasicNameValuePair("examid",k.trim()));
				
	            	
	            	InputStream is =null;
	            	String result = "";
	            	
	            	
	            	//Sending Information to server
	            	try {
	            	    
	            		HttpClient httpclient = new DefaultHttpClient();
	                    HttpPost httppost = new HttpPost("http://10.0.2.2:8080/RegisterServer/ViewMarks");
	                    httppost.setEntity(new UrlEncodedFormEntity(postParameters));
	                    HttpResponse response = httpclient.execute(httppost);
	                    HttpEntity entity = response.getEntity();
	                    is = entity.getContent();         	    
	            	} 
	            	catch (Exception e) {
	            		
	            		Toast.makeText(viewmarks1Activity.this,"Error in http connection : "+e.toString(), Toast.LENGTH_LONG).show();
	            	}
	            	
	            	//Processing results given by server
	            	//convert response to string
	                try{
	                        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	                        StringBuilder sb = new StringBuilder();
	                        String line = null;
	                        while ((line = reader.readLine()) != null) {
	                                sb.append(line + "\n");
	                        }
	                        is.close();
	                 
	                        result=sb.toString();
	                        Toast.makeText(viewmarks1Activity.this,"Message : "+result+result.length(), Toast.LENGTH_LONG).show();
	                    
	                        /*End of Validation*/
	    					                
	                }
	                catch (Exception e) 
	                {
	            		
	            		Toast.makeText(viewmarks1Activity.this,"Error converting result : "+e.toString(), Toast.LENGTH_LONG).show();
	            	}
	            	

		
				    //end of servlet

					
					
				}
			});
	        
	    	
	       
	 }

}
