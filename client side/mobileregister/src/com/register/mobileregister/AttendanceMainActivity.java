package com.register.mobileregister;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class AttendanceMainActivity extends ListActivity {

	
/** Called when the activity is first created. */
	
	public List<Model> listy;
	Button submit;
	String count,userid,semid,secid,subid,result,present_list="";
	String[] std_list;

	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Bundle bu=getIntent().getExtras();
        result=bu.getString("result");
        Toast.makeText(AttendanceMainActivity.this,result, Toast.LENGTH_LONG).show();
        
		listy = getModel(result);
		// Create an array of Strings, that will be put to our ListActivity
		ArrayAdapter<Model> adapter = new InteractiveArrayAdapter(this,
				listy);
		
		
		 
	
	       
	       
	       
		
		//to display button at bottom
		View footer = getLayoutInflater().inflate(R.layout.footer, null);
		ListView listView = getListView();		
		listView.addFooterView(footer);
		
		
		setListAdapter(adapter);
		
		submit=(Button)findViewById(R.id.submit);
		
		submit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				for(int i=0;i<listy.size();i++)
				{	
					if(listy.get(i).isSelected())
					{	
						present_list=present_list+listy.get(i).getName()+"*";
						Toast.makeText(AttendanceMainActivity.this,"Name : "+listy.get(i).getName(), Toast.LENGTH_LONG).show();
					}
				}
				
				//code to servlet
				
				
					
					ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
					postParameters.add(new BasicNameValuePair("userid",userid.trim()));
					postParameters.add(new BasicNameValuePair("semid",semid.trim()));
					postParameters.add(new BasicNameValuePair("secid",secid.trim()));
					postParameters.add(new BasicNameValuePair("subid",subid.trim()));
					postParameters.add(new BasicNameValuePair("present",present_list.trim()));
					
	            	
	            	InputStream is =null;
	            	String result = "";
	            	
	            	
	            	//Sending Information to server
	            	try {
	            	    
	            		HttpClient httpclient = new DefaultHttpClient();
	                    HttpPost httppost = new HttpPost("http://10.0.2.2:8080/RegisterServer/PostAttendance");
	                    httppost.setEntity(new UrlEncodedFormEntity(postParameters));
	                    HttpResponse response = httpclient.execute(httppost);
	                    HttpEntity entity = response.getEntity();
	                    is = entity.getContent();         	    
	            	} 
	            	catch (Exception e) {
	            		
	            		Toast.makeText(AttendanceMainActivity.this,"Error in http connection : "+e.toString(), Toast.LENGTH_LONG).show();
	            	}
	            	
	            	//Processing results given by server
	            	//convert response to string
	                try{
	                        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	                        StringBuilder sb = new StringBuilder();
	                        String line = null;
	                        while ((line = reader.readLine()) != null) {
	                                sb.append(line + "\n");
	                        }
	                        is.close();
	                 
	                        result=sb.toString();
	 //                       Toast.makeText(MobileregisterActivity.this,"Message : "+result+result.length(), Toast.LENGTH_LONG).show();
	                    
	                        /*End of Validation*/
	    					                

					
				}
				catch(Exception e)
				{
					
				}
				
				//end of code to servlet
				
			}});	
	                
	}	
		

	private List<Model> getModel(String res) {
		
		//
		
		List<Model> list = new ArrayList<Model>();
		std_list=res.split("[*]");
        for(int i=0;i<std_list.length;i++)
        {
        	list.add(get(std_list[i]));
        	list.get(i).setSelected(true);
        }
		
        
		
//		list.add(get("Linux"));
//		list.add(get("Windows7"));
//		list.add(get("Suse"));
//		list.add(get("Eclipse"));
//		list.add(get("Ubuntu"));
//		list.add(get("Solaris"));
//		list.add(get("Android"));
//		list.add(get("iPhone"));
//		list.add(get("Android2"));
//		list.add(get("iPhone2"));
//		list.add(get("Android3"));
//		list.add(get("iPhone3"));
//		// Initially select one of the items
		
		return list;
	}

	private Model get(String s) {
		return new Model(s);
	}
	
		

}
