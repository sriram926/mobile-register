package com.register.mobileregister;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class lecturer2Activity extends Activity {
	Button btnTakeAttend,btnViewAttend,btnViewMarks,btnAnalyAttend;
	String userid,sem_id,sub_id,sec_id;
	 public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.lecturer2);
	        
	        Bundle bu=getIntent().getExtras();
       String cls_details=bu.getString("cls_selected");
	        userid=bu.getString("userid");
//	        Toast.makeText(lecturer2Activity.this,cls_details, 10).show();
	        
	        String[] sub_temp=cls_details.split("[+]");
	        sub_id=sub_temp[0];
	        
	        String[] sem_temp=sub_temp[1].split("[*]");
	        sem_id=sem_temp[0];
	        sec_id=sem_temp[1];
//	        Toast.makeText(lecturer2Activity.this,"Sub"+sub_id+" Sem"+sem_id+" Sec"+sec_id, 10).show();
	        
	        
	        
	        //Take attendance module
	                
	        btnTakeAttend=(Button)findViewById(R.id.btnTakeAttn);
	        btnTakeAttend.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					try
					{
						Intent takeAttend=new Intent(lecturer2Activity.this,lecturerTakeAttendanceActivity.class);
//						
						Bundle bun=new Bundle();
						bun.putString("userid", userid);
						bun.putString("subid", sub_id);
						bun.putString("semid", sem_id);
						bun.putString("secid", sec_id);//add selected class here
						takeAttend.putExtras(bun);
						
						
						//start of servlet
						
						/* start of Validation */
						
						//creating name values pair to send data to server
						ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
						postParameters.add(new BasicNameValuePair("userid",userid.trim()));
						postParameters.add(new BasicNameValuePair("semid",sem_id.trim()));
						postParameters.add(new BasicNameValuePair("subid",sub_id.trim()));
						postParameters.add(new BasicNameValuePair("secid",sec_id.trim()));
					
		            	
		            	InputStream is =null;
		            	String result = "";
		            	
		            	
		            	//Sending Information to server
		            	try {
		            	    
		            		HttpClient httpclient = new DefaultHttpClient();
		                    HttpPost httppost = new HttpPost("http://10.0.2.2:8080/RegisterServer/TakeAttendance");
		                    httppost.setEntity(new UrlEncodedFormEntity(postParameters));
		                    HttpResponse response = httpclient.execute(httppost);
		                    HttpEntity entity = response.getEntity();
		                    is = entity.getContent();         	    
		            	} 
		            	catch (Exception e) {
		            		
		            		Toast.makeText(lecturer2Activity.this,"Error in http connection : "+e.toString(), Toast.LENGTH_LONG).show();
		            	}
		            	
		            	//Processing results given by server
		            	//convert response to string
		                try{
		                        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
		                        StringBuilder sb = new StringBuilder();
		                        String line = null;
		                        while ((line = reader.readLine()) != null) {
		                                sb.append(line + "\n");
		                        }
		                        is.close();
		                 
		                        result=sb.toString();
		 //                       Toast.makeText(MobileregisterActivity.this,"Message : "+result+result.length(), Toast.LENGTH_LONG).show();
		                    
		                        /*End of Validation*/
		    					                
		                }
		                catch (Exception e) 
		                {
		            		
		            		Toast.makeText(lecturer2Activity.this,"Error converting result : "+e.toString(), Toast.LENGTH_LONG).show();
		            	}
		            	

			
					    //end of servlet
						
//						
						startActivity(takeAttend);
					}
					catch(Exception e)
					{
						Toast.makeText(lecturer2Activity.this,"EXC" + e.toString(), 10).show();
					}
					
				}
			});
	        
	              
	       
	        //View Attendance Module
	        
	        btnViewAttend=(Button)findViewById(R.id.btnViewAttn);
	        btnViewAttend.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					try
					{
						Intent viewAttnd=new Intent(lecturer2Activity.this,viewattnd1Activity.class);
						
						Bundle bun=new Bundle();
						bun.putString("cls_selected", "4-1");//add selected class here
						viewAttnd.putExtras(bun);
						
						startActivity(viewAttnd);
					}
					catch(Exception e)
					{
						Toast.makeText(lecturer2Activity.this,"EXC" + e.toString(), 10).show();
					}
					
				}
			});

	        
	   	        
	        //View Marks Module
	        
	        btnViewMarks=(Button)findViewById(R.id.btnMarkView);
	        btnViewMarks.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					try
					{
						Intent viewMarks=new Intent(lecturer2Activity.this,viewmarks1Activity.class);
						
						Bundle bun=new Bundle();
						bun.putString("userid", userid);//add selected class here
						bun.putString("subid", sub_id);
						bun.putString("semid", sem_id);
						bun.putString("secid", sec_id);//add selected class here
						
						
						viewMarks.putExtras(bun);
						
						startActivity(viewMarks);
					}
					catch(Exception e)
					{
						Toast.makeText(lecturer2Activity.this,"EXC" + e.toString(), 10).show();
					}
					
				}
			});
	        
	 	        
	        //Attendance Analysis Module
	        
	        btnAnalyAttend=(Button)findViewById(R.id.btnAnalyAttend);
	        btnAnalyAttend.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					try
					{
						Intent attndAnaly=new Intent(lecturer2Activity.this,attndanaly1Activity.class);
						
						Bundle bun=new Bundle();
						bun.putString("cls_selected", "4-1");//add selected class here
						attndAnaly.putExtras(bun);
						
						startActivity(attndAnaly);
					}
					catch(Exception e)
					{
						Toast.makeText(lecturer2Activity.this,"EXC" + e.toString(), 10).show();
					}
					
				}
			});

       
	 }
}
