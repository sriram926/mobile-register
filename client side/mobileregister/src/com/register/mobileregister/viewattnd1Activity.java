package com.register.mobileregister;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

public class viewattnd1Activity extends Activity {
	Button btnViewAttend;
	DatePicker d;
	
	
	 public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.viewattnd1);
	        
	        d=(DatePicker)findViewById(R.id.datePicAttendance);
	        
	        
	        
	        Bundle bu=getIntent().getExtras();
	        String str=bu.getString("cls_selected");
	        Toast.makeText(viewattnd1Activity.this,str, 10).show();
	       
	        
	        btnViewAttend=(Button)findViewById(R.id.btnShowAttend);
	        btnViewAttend.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					try
					{
						Intent viewAttend=new Intent(viewattnd1Activity.this,viewattnd2Activity.class);
						startActivity(viewAttend);
					}
					catch(Exception e)
					{
						Toast.makeText(viewattnd1Activity.this,"EXC" + e.toString(), 10).show();
					}
					
				}
			});
	        
	 }

}
