package com.register.mobileregister;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class student1Activity extends Activity  {
	
	Button btnStudId;
	EditText edtStdId;
	String stdid;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.student1);
        
        btnStudId=(Button)findViewById(R.id.btnStudentLogin_Id);
        edtStdId=(EditText)findViewById(R.id.edtStdId);
     //   Toast.makeText(student1Activity.this,edtStdId.toString(), 10).show();
        stdid=edtStdId.getText().toString().trim();
       
        btnStudId.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				try
				{
					Intent in=new Intent(student1Activity.this,student2Activity.class);
					
				Bundle bu=new Bundle();
					bu.putString("cls_selected", "4-1");//add selected class here
					bu.putString("userid", edtStdId.getText().toString().trim());
					in.putExtras(bu);
					
					
					
					startActivity(in);
				}
				catch(Exception e)
				{
					Toast.makeText(student1Activity.this,"Exc : "+e.toString(), 10).show();
				}
			}	
		});
        
    
	}
}
