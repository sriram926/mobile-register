package com.register.mobileregister;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class attndanaly1Activity extends Activity {
	Button btnAttendAll,btnAttendModerate,btnAttendLow;
	 public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.attndanaly1);
	        
	        Bundle bu=getIntent().getExtras();
	        String str=bu.getString("cls_selected");
	        Toast.makeText(attndanaly1Activity.this,str, 10).show();
	       
	        
	        
	        //Attendance analysis for all students
	        
	        btnAttendAll=(Button)findViewById(R.id.btnAttndAll);
	        btnAttendAll.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					try
					{
						Intent AttendAll=new Intent(attndanaly1Activity.this,attnd_analy_allActivity.class);
						startActivity(AttendAll);
					}
					catch(Exception e)
					{
						Toast.makeText(attndanaly1Activity.this,"EXC" + e.toString(), 10).show();
					}
					
				}
			});
	 
	 
	        //Attendance analysis for 65% - 75% students
	        
	        btnAttendModerate=(Button)findViewById(R.id.btnAttndModerate);
	        btnAttendAll.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					try
					{
						Intent AttendModerate=new Intent(attndanaly1Activity.this,attnd_analy_moderateActivity.class);
						startActivity(AttendModerate);
					}
					catch(Exception e)
					{
						Toast.makeText(attndanaly1Activity.this,"EXC" + e.toString(), 10).show();
					}
					
				}
			});
	        
	        
	        //Attendance analysis for 65% below students
	        
	        btnAttendLow=(Button)findViewById(R.id.btnAttndLow);
	        btnAttendLow.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					try
					{
						Intent AttendLow=new Intent(attndanaly1Activity.this,attnd_analy_lowActivity.class);
						startActivity(AttendLow);
					}
					catch(Exception e)
					{
						Toast.makeText(attndanaly1Activity.this,"EXC" + e.toString(), 10).show();
					}
					
				}
			});
	 
	 }

}
