package com.register.mobileregister;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class lecturerHomeActivity extends Activity {
	Button btn;String userid;
	Spinner lecturer_clases;
	String each_class[];

	 public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.lecturerhome);
	        btn=(Button)findViewById(R.id.btnClass);
	        lecturer_clases= (Spinner) findViewById(R.id.spinClasses);
	       
	        Bundle bun=getIntent().getExtras();
	        userid=bun.getString("userid");
	        Toast.makeText(lecturerHomeActivity.this,userid, 10).show();
	        

	        
	        ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
			postParameters.add(new BasicNameValuePair("userid",userid));
			
        	
        	InputStream is =null;
        	String result = "";
        	
        	
        	//Sending Information to server
        	try {
        	    
        		HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://10.0.2.2:8080/RegisterServer/LecturerClassesServlet");
                httppost.setEntity(new UrlEncodedFormEntity(postParameters));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();         	    
        	    } 
        	catch (Exception e) {
        		
        		Toast.makeText(lecturerHomeActivity.this,"Error in http connection : "+e.toString(), Toast.LENGTH_LONG).show();
        	    }
        	
        	
        	//Processing results given by server
        	//convert response to string
            try{
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                            sb.append(line + "\n");
                    }
                    is.close();
             
                    result=sb.toString().trim();
            
                    //splitting the result string  into parts
                    
                    each_class=result.split("[$]");
                  
                   String[] sub_temp,sem_temp;
                   String[] cls_list = new String[each_class.length];
                   String sub_id,sem_id,sec_id;
                   
//                   
//                   
                   for(int i=0;i<each_class.length;i++)
                    {
  //                  	    cls_list[i]="";
                    	sub_temp=each_class[i].split("[+]");
             	        sub_id=sub_temp[0];
             	        
             	        sem_temp=sub_temp[1].split("[*]");
             	        sem_id=sem_temp[0];
             	        sec_id=sem_temp[1];
             	        cls_list[i]=sub_id.toUpperCase()+" "+sem_id+"Sem "+sec_id;
                    }
                    
        	        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,cls_list);
        	        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        	        lecturer_clases.setAdapter(adapter);
        	        Toast.makeText(lecturerHomeActivity.this,"Message : "+each_class[lecturer_clases.getSelectedItemPosition()], Toast.LENGTH_LONG).show();
        	        
//                  Toast.makeText(lecturerHomeActivity.this,"Message : "+result+result.length(), Toast.LENGTH_LONG).show();
            }
            catch(Exception e)
            {
            	 Toast.makeText(lecturerHomeActivity.this,"Message : "+result+result.length(), Toast.LENGTH_LONG).show();
            }
	        
	        
	        
	        
	        btn.setOnClickListener(new View.OnClickListener() 
	        {
				
				@Override
				public void onClick(View v) 
				{
					
					try
					{
						Intent in=new Intent(lecturerHomeActivity.this,lecturer2Activity.class);
						
						//sending the selected class to next screen
						Bundle bu=new Bundle();
						bu.putString("userid", userid);//add selected class here
						in.putExtras(bu);	

						
						in.putExtra("cls_selected",each_class[lecturer_clases.getSelectedItemPosition()]);
						startActivity(in);
					}
					catch(Exception e)
					{
						Toast.makeText(lecturerHomeActivity.this,"Exc : "+e.toString(), 10).show();
					}
				}
			});
	        

	 }
}