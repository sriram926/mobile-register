package com.register.mobileregister;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class lecturerTakeAttendanceActivity extends Activity {
	Button btnLab,btnClass;
	String userid,semid,secid,subid,result;
	InputStream is =null;
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lecturertakeattnd);
        btnLab=(Button)findViewById(R.id.btnLab);
        btnClass=(Button)findViewById(R.id.btnClass);
        
        Bundle bu=getIntent().getExtras();
        userid=bu.getString("userid");
        semid=bu.getString("semid");
        secid=bu.getString("secid");
        subid=bu.getString("subid");
       
        if(secid.equalsIgnoreCase("s1"))
        	secid="1";
        if(secid.equalsIgnoreCase("s2"))
        	secid="2";
       
         Toast.makeText(lecturerTakeAttendanceActivity.this,userid, 10).show();
		
        
 btnLab.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				try
				{
					 
//					bund.putString("count", "3");//add selected class here
//					bund.putString("userid", userid);
//					bund.putString("subid", subid);
//					bund.putString("semid", semid);
//					bund.putString("secid", secid);//add selected class here
//					
//					in.putExtras(bund);
					
					//start paste
					

					ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
			    	postParameters.add(new BasicNameValuePair("userid",userid));
			    	postParameters.add(new BasicNameValuePair("subid",subid));
			    	postParameters.add(new BasicNameValuePair("semid",semid));
			    	postParameters.add(new BasicNameValuePair("secid",secid));
//			    	postParameters.add(new BasicNameValuePair("count",count));
					
			    	
			    	
			    	
			    	//Sending Information to server
//			    	try {
			    	    
			    		HttpClient httpclient = new DefaultHttpClient();
			            HttpPost httppost = new HttpPost("http://10.0.2.2:8080/RegisterServer/TakeAttendance");
			            httppost.setEntity(new UrlEncodedFormEntity(postParameters));
			            HttpResponse response = httpclient.execute(httppost);
			            HttpEntity entity = response.getEntity();
			            is = entity.getContent();         	    
			    } 
			    	catch (Exception e) {
			    		
			    		Toast.makeText(lecturerTakeAttendanceActivity.this,"Error in http connection : "+e.toString(), Toast.LENGTH_LONG).show();
			    	    }
			    	
			    	
			    	//Processing results given by server
			    	//convert response to string
			        try{
			        	
			        	
				    	result = "";
				    	
			                BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			                StringBuilder sb = new StringBuilder();
			                String line = null;
			                while ((line = reader.readLine()) != null) {
			                        sb.append(line + "\n");
			                }
			                is.close();
			                result=sb.toString().trim();
			                Toast.makeText(lecturerTakeAttendanceActivity.this,result, Toast.LENGTH_LONG).show();
			                
			                try{
			                	Intent in;
			                	Bundle bund;
			                				                	
			                	in=new Intent(lecturerTakeAttendanceActivity.this,AttendanceMainActivity.class);
								bund=new Bundle();
							    
								bund.putString("result", result);
								bund.putString("semid", semid);
								bund.putString("secid", secid);
								bund.putString("userid", userid);
								bund.putString("subid", subid);
								in.putExtras(bund);
								startActivity(in);
			                }
			                catch(Exception e)
			                {
			                	Toast.makeText(lecturerTakeAttendanceActivity.this,e.getMessage().toString(), Toast.LENGTH_LONG).show();
			                }
					//end paste
					
					
					
				}
				catch(Exception e)
				{
					Toast.makeText(lecturerTakeAttendanceActivity.this,"EXC" + e.toString(), 10).show();
				}
				
			}
		});

 btnClass.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			try
			{
				
				Intent in=new Intent(lecturerTakeAttendanceActivity.this,AttendanceMainActivity.class);
				
				Bundle bun=new Bundle();
				bun.putString("count", "1");//add selected class here
				bun.putString("userid", userid);
				bun.putString("subid", subid);
				bun.putString("semid", semid);
				bun.putString("secid", secid);//add selected class here
				
				in.putExtras(bun);
				
				
				startActivity(in);
			}
			catch(Exception e)
			{
				Toast.makeText(lecturerTakeAttendanceActivity.this,"EXC" + e.toString(), 10).show();
			}
			
		}
	});


	}

}
