/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.register;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 *
 * @author indupriya
 */
public class ViewAttendance extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession(true);
        try {
            String un,subid,secid,semid,examid_temp;
            un=request.getParameter("userid");
            subid=request.getParameter("subid");
           semid=request.getParameter("semid");
           secid=request.getParameter("secid");
//           examid_temp=request.getParameter("examid");
           
           
           
           int sec,sem,examid;
           
 //          examid=Integer.parseInt(examid_temp);
           sec=Integer.parseInt(secid);
           sem=Integer.parseInt(semid);
            
            Class.forName("org.postgresql.Driver");

            String jdbcUrl = DBDetails.jdbcUrl;

            String dbuserid = DBDetails.dbuserid;// set ur userid
            String dbpassword = DBDetails.dbpassword; // set ur password

            /* 3.Establish the connection */
            Connection conn = DriverManager.getConnection(jdbcUrl, dbuserid, dbpassword);
            //Connection conn = DriverManager.getConnection(DBDetails.connectionURL);
            
           
             Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
//            String query = "select * from assign_class where lid='"+un+"'";
            String query = "select * from students; ";
            String stds_marks_info="";
            ResultSet students = stmt.executeQuery(query);
            while(students.next())
            {
                stds_marks_info=stds_marks_info+students.getString(2)+"*"+students.getInt(5)+"$";
            }
            out.println(stds_marks_info);
            
            stmt.close();
            conn.close();
        }
        catch(SQLException e)
        {
            //out.print(0);
            //out.println("<br><b>SQLException :"+e.getMessage()+"</b>");
            String finalresult = "result-"+"SQLException :"+e.getMessage();
            out.print(finalresult);
            //out.flush();
        }
        catch(ClassNotFoundException e)
        {
            //out.print(0);
            //out.println("<br><b>ClassNotException :"+e.getMessage()+"</b>");
            String finalresult = "result-"+"ClassNotException :"+e.getMessage();
            out.print(finalresult);
            out.flush();
        }
        catch(Exception e)
        {
            out.println("Exception"+e.getMessage());
        }
                   
 finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
