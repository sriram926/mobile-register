/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.register;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author indupriya
 */
public class LecturerClassesServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession(true);
        try {
            String un;
            un=request.getParameter("userid");
            
           
            
            Class.forName("org.postgresql.Driver");

            String jdbcUrl = DBDetails.jdbcUrl;

            String dbuserid = DBDetails.dbuserid;// set ur userid
            String dbpassword = DBDetails.dbpassword; // set ur password

            /* 3.Establish the connection */
            Connection conn = DriverManager.getConnection(jdbcUrl, dbuserid, dbpassword);
            //Connection conn = DriverManager.getConnection(DBDetails.connectionURL);
            
           
             Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
//            String query = "select * from assign_class where lid='"+un+"'";
            String query = "select * from assign_class a,subjects s where a.subid=s.subid and a.lid='"+un+"'";

            ResultSet assign_info = stmt.executeQuery(query);
            String sem_section_info="";
           
//            assign_info.next();
//            out.println(assign_info.getString(1));
           while(assign_info.next())
           {
               sem_section_info=sem_section_info+assign_info.getString(1)+"+"+assign_info.getInt(6)+"*"+"S"+assign_info.getInt(3)+"$";
           }            
           
            out.println(sem_section_info);
            stmt.close();
            conn.close();
        }
        catch(SQLException e)
        {
            //out.print(0);
            //out.println("<br><b>SQLException :"+e.getMessage()+"</b>");
            String finalresult = "result-"+"SQLException :"+e.getMessage();
            out.print(finalresult);
            //out.flush();
        }
        catch(ClassNotFoundException e)
        {
            //out.print(0);
            //out.println("<br><b>ClassNotException :"+e.getMessage()+"</b>");
            String finalresult = "result-"+"ClassNotException :"+e.getMessage();
            out.print(finalresult);
            out.flush();
        }
                   
         
        
        finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
